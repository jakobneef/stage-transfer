# Britgrav 2024

Presentation for my PhD stage transfer at UCD on 30th of April 2024.
This presentation was generated using impress.js.
This work was supported from Science Foundation Ireland under Grant number 21/PATH-S/9610.
You can find the presentation under:
https://jakobneef.gitlab.io/stage-transfer/
